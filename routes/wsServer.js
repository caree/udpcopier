
var http = require('http');
var WebSocketServer = require('ws').Server;
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;

var server;
global.wss;
var wsServer = null;
var clients = [];

g_ep.tail('msg', broadcastTagInfo);

function broadcastTagInfo(msg){
	console.log((timeFormater() + '  ' + msg).data);
	// console.dir(_record);
	wsServer.broadcast(msg);
}
exports.startWebSocketServer = function(app){

	server = http.createServer(app);
	wsServer = new WebSocketServer({server : server});
	wsServer.broadcast = function(data) {
		_.each(clients, function(_client){
	        _client.send(data);
		});
	};
	wsServer.on('connection', function(ws){
		clients.push(ws);
	    console.log('connected'.info);
		ws.on('open', function(){
		    console.log('connected'.info);
		  });
		
		ws.on('message', function(msg){
		    console.log(('message => '+ msg).data);
		  });
		
		ws.on('close', function(){
		    var index = clients.indexOf(ws);
		    clients.splice(index, 1);
		    console.log('close =>'.info);
		  });
		});	

	return server;
}




