
var _          = require('underscore');
var dgram = require("dgram");
var timeFormater = require('./timeFormat').getCurrentTime;



startCopier();

exports.index = function(req, res){
	res.render('index', {_udpList: JSON.stringify(updList)});
	// res.render('index', {title:'EPC公共信息管理系统', _system_name:'EPC公共信息管理系统', _company:'北京科技发展有限公司'});
}
exports.show = function(req, res){
	res.render('wsTest');
}
function startCopier(){
	_.each(updList, function(_udpConfig){
		startServer(_udpConfig.from, _udpConfig.to);
	});
}

function startServer(_listeningPort, _destList){
	var server = dgram.createSocket("udp4");

	server.on("error", function (err) {
	  console.log("server error:\n" + err.stack);
	  server.close();
	});

	server.on("message", function (msg, rinfo) {
		var log = timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port;
	  	console.log(log.data);
	  	g_ep.emit('msg', log);
	  	// console.log(timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port);
		_.each(_destList, function(_dest){
			var client = dgram.createSocket("udp4");
			var copyLog = 'will copy to '+ _dest.ip + ':'+_dest.port; 
			console.log(copyLog.data);
			g_ep.emit('msg', copyLog);
			client.send(msg, 0, msg.length, _dest.port, _dest.ip, function(err, bytes) {
			  client.close();
			});
		});
	});

	server.on("listening", function () {
	  var address = server.address();
	  console.log("server listening " +
	      address.address + ":" + address.port);
	});

	server.bind(_listeningPort);
	return server;
}



