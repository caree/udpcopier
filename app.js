
/**
 * Module dependencies.
 */
require('./config')
var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
var EventProxy = require('eventproxy');
g_ep = new EventProxy();

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');

var wsServer = require('./routes/wsServer');

var app = express();

var server = wsServer.startWebSocketServer(app);


// all environments
app.set('port', process.env.PORT || httpListeningPort);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.get('/index', routes.index);
app.get('/', routes.show);
// app.get('/indexapi', routes.indexAPI);


server.listen(app.get('port'), function(){
  console.log(('UDP Copier server listening on port ' + app.get('port')).info);
});
